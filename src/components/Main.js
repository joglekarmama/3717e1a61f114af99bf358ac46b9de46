import React, { useEffect, useState } from 'react'
import config from 'visual-config-exposer'
export default function Main() {
   
  var array =[]
    var message = [{...config.settings.messages}][0]
    var array2 = Object.entries(message)
    var messages
  var sentence=''
  var space=' '
  const font = config.settings.font;
    const position = config.settings.position
    const boxColor = config.settings.boxColor
    const autoplay = config.settings.autoplayTime
    const textColor = config.settings.textColor
    const height = config.settings.size.height
    const width = config.settings.size.width
    const time = autoplay/1000
    console.log(time)
   

    
    
  
      
            var i=0
            while(i<array2.length){
                messages = Object.entries(message)[i][1].message
                array.push(messages)
                sentence+=messages+space
                i++
            }
           
            var j=0
            var text = array[j]
            const [word, setWord] = useState(text)
            const [count, setCount] = useState(0)

     useEffect(() =>{
       
        const elements = document.getElementsByClassName('text')[0]
        var element = document.createElement("link");
        element.setAttribute("rel", "stylesheet");
    element.setAttribute("type", "text/css");
    element.setAttribute("href", "https://fonts.googleapis.com/css?family="+font);
    document.getElementsByTagName("head")[0].appendChild(element)
    elements.style.fontFamily=font
    elements.style.backgroundColor=boxColor
    elements.style.color = textColor
    elements.style.height = height+'px'
    elements.style.width = width+'vw'
    /*elements.style.boxShadow='5px 2px 5px '+boxColor*/

        if(position=='top'){
            elements.style.top='10%'
            elements.style.left='50%'
        elements.style.transform='translate(-50%, -50%)'

    }
    if(position=='medium'){
       elements.style.top='50%'
       elements.style.left='50%'
        elements.style.transform='translate(-50%, -50%)'
}
if(position=='bottom'){
   elements.style.top='80%'
   elements.style.left='50%'
   elements.style.transform='translate(-50%, -50%)'

}           
        if(count<array.length+1){
            setTimeout(() => {
                document.getElementsByClassName('word')[0].style.animation= 'slide 2s ease-in infinite'
                check()
            }, autoplay);
           
        }
        else{
            setWord(sentence)
            document.getElementsByClassName('word')[0].style.animation= ''
        }
        function check() {

            setCount(count+1)
            setWord(array[count])
          
        }
     })
          
      
            
    return(
        <>
        <div className='text'>
    <p className='word'>{word}</p>
        </div>
        </>
    )
}
